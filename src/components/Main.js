
const Main = ({activeNote, updateNote, category}) => {
    const onEdit = (key, value) => {
        updateNote({
            ...activeNote,
            [key]: value,
            lastModified: Date.now()
        });
    };


    if (!activeNote) return <div className="no-active-note">No note selected</div>

    return (
        <div className="app-main">
            <div className="app-sidebar-header">
                <h1>My note</h1>
            </div>
            <div className="app-main-note-edit">
                <input type="text"
                       id="title"
                       placeholder="Write your note title here ..."
                       value={activeNote.title}
                       onChange={(e) => onEdit("title", e.target.value)}
                       autoFocus/>
                <div>{category}</div>
                <textarea id="body"
                          value={activeNote.body}
                          placeholder="Write your note body here ..."
                          onChange={(e) => onEdit("body", e.target.value)}/>
            </div>
        </div>

    )
}
export default Main;