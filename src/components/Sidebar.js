import {FaPlus, FaTrash} from "react-icons/fa";
import {MdSearch} from "react-icons/md";

const Sidebar = ({notes, search, newNote, deleteNote, activeNote, setActiveNote}) => {
    const sortNotes = notes.sort((old,last) => last.lastModified - old.lastModified);

    return (
        <div className="app-sidebar">
            <div className="app-sidebar-header">
                <h1>All notes</h1>
                <button onClick={newNote}><FaPlus /> Add new </button>
            </div>
            <div className="search">
                <MdSearch className="search-icons" size="1.5em"/>
                <input type="text" onChange={(e) => search(e.target.value)} placeholder="type to search..."/>
            </div>
            <div className="app-sidebar-notes">
                {sortNotes.map((note) => {
                        return(
                            <div key={note.id} className={`app-sidebar-note ${note.id === activeNote && "active"}`}
                                 onClick={() => setActiveNote(note.id)}>
                                <div className="sidebar-note-title">
                                    <strong>{note.title}</strong>
                                    <button onClick={() => deleteNote(note.id)}> <FaTrash className="delete-icon"/> </button>
                                </div>

                                <p>{note.body && note.body.substr(0, 100) + "..."}</p>

                                <div className="category"> {note.category}</div>

                                <small className="note-meta">{new Date(note.lastModified).toLocaleDateString("en-GB", {
                                    hour: "2-digit",
                                    minute: "2-digit"
                                })}
                                </small>
                            </div>
                        )
                    }
                )}
            </div>
        </div>
    )
}

export default Sidebar;