import {useEffect, useState} from "react";
import Sidebar from "../components/Sidebar";
import Main from "../components/Main";
import {v1 as uuid}  from "uuid";
import Select from "react-select";

const Notes = () => {
    const [notes, setNotes] = useState( localStorage.notes ? JSON.parse(localStorage.notes) : []);
    const [activeNote, setActiveNote] = useState(false);
    const [search, setSearch] = useState('');
    const [choosenCategory, setChoosenCategory] = useState('');

    const optionsCategory = [
        {value: 'Radio', label: 'Radio'},
        {value: 'Learning', label: 'Learning'},
        {value: 'Date', label: 'Date'},
        {value: 'Gossip', label: 'Gossip'},
        {value: 'Business', label: 'Business'}
    ]

    useEffect(() => {
        localStorage.setItem("storeNotes", JSON.stringify(notes))
    }, [notes])

    const onAdd = () => {
        const addNote = {
            id: uuid(),
            title: "Title",
            category: "",
            body: "",
            lastModified: Date.now()
        }
        setNotes([addNote, ...notes]);
        setActiveNote(addNote.id);
    }

    const onDelete = (idOfNote) => {
        setNotes(notes.filter((data) => data.id !== idOfNote));
    }

    const onUpdate = (updatedNote) => {
        const updatedNotesArray = notes.map((note) => {
            if(note.id === activeNote) {
                return updatedNote;
            }

            return note;
        });
       setNotes(updatedNotesArray);
    };


    const getActiveNote = () => {
        return notes.find((data) => data.id === activeNote)
    }

    const chooseCategory = () => {
        return(
            <>
                <p className="mb-0">Choose category type:</p>
                <Select
                    isDisabled={choosenCategory.value}
                    value={choosenCategory}
                    options={optionsCategory}
                    onChange={(e) => {
                        setChoosenCategory(e);

                        setNotes(notes.map(note => {
                            if (note.id === activeNote) {
                                return {
                                    ...note,
                                    category: e.value
                                };
                            }
                            return note;
                        }))
                    }}
                />
            </>
        )
    };

    useEffect(() => {
        const selectedNoteCategory = getActiveNote()?.category || ''
        const activeCategory = optionsCategory.find(option => option.value === selectedNoteCategory);
        setChoosenCategory(activeCategory ? activeCategory : {});
    }, [activeNote]);



    return (
        <div className="Notes">
            <Sidebar notes={notes.filter((note) => note.title.toLowerCase().includes(search) ||
                                                   note.body.toLowerCase().includes(search))}
                     search={setSearch}
                     newNote={onAdd}
                     deleteNote={onDelete}
                     activeNote={activeNote}
                     setActiveNote={setActiveNote}/>
            <Main activeNote={getActiveNote()}
                  updateNote={onUpdate}
                  category={chooseCategory()}
                  />
        </div>
    );
}

export default Notes;
